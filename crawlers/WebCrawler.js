let Crawler = require('./Crawler'),
    rp = require('request-promise'),
    request = require('request'),
    dom = require('xmldom').DOMParser,
    xpath = require('xpath'),
    { validate, checkParser } = require('../utils/checks'),
    { sleep } = require('../utils/general'),
    XPath = require('../utils/xpath')

class WebCrawler extends Crawler {
    constructor(config) {
        super(config)
        this.domOpts = {
            errorHandler: {
                warning: warning => { },
                error: error => { },
                fatalError: fatal => { }
            }
        }
    }

    start() {
        console.log('Starting Crawler...')
        return this.crawl(this.config.params)
    }

    crawl(params) {
        this.status.processed.push(params.url)
        return new Promise((resolve, reject) => {
            request(params, (err, resp, body) => {
                if (err)
                    reject(err)
                else {
                    let document = new dom(this.domOpts).parseFromString(body)
                    let links = xpath.select('//a/@href', document).map(link => link.value)
                    resolve(links.map(link => /^https?:\/\//i.test(link) ?
                        link : `${resp.request.uri.protocol}//${resp.request.uri.host}${link}`)
                        .reduce((promiseChain, link) =>
                            promiseChain.then(() => {
                                if (!this.status.processed.includes(link)) {
                                    if (this.isAllowed(link)) {
                                        console.log(link)
                                        if (this.isIgnored(link)) {
                                            this.status.processed.push(link)
                                            this.status.ignore.push(link)
                                            this.report()
                                            return Promise.resolve()
                                        } else if (this.isScraped(link)) {
                                            sleep(this.config.delay * 1000)
                                            this.status.processed.push(link)
                                            this.status.scrape.push(link)
                                            this.report()
                                            return this.parse(link)
                                        } else if (this.isFollowed(link)) {
                                            sleep(this.config.delay * 1000)
                                            this.status.follow.push(link)
                                            this.report()
                                            return this.crawl({
                                                ...params,
                                                url: link
                                            })
                                        } else {
                                            this.status.processed.push(link)
                                            this.status.ignore.push(link)
                                            this.report()
                                            return Promise.resolve()
                                        }
                                    } else {
                                        this.status.processed.push(link)
                                        this.status.disallow.push(link)
                                        this.report()
                                        return Promise.resolve()
                                    }
                                }
                            }).catch(err => { console.error(err) }), Promise.resolve()))
                }
            })
        }).catch(err => {
            console.error('ERROR', err)
            new Promise((resolve, reject) => {
                resolve(sleep(5000))
            }).then(this.crawl(params))
        })
    }

    parse(link) {
        return new Promise((resolve, reject) => {
            request({
                ...this.config.params,
                uri: link
            }, (err, res, body) => {
                if (err)
                    reject(err)
                else {
                    let doc = new dom(this.domOpts).parseFromString(body)
                    let keys = Object.keys(this.config.parse)
                    resolve(this.config.output.write(Object.assign({}, keys.reduce((acc, key) => {
                        let parser = this.config.parse[key]
                        checkParser(key, parser)
                        if (typeof parser.xPath === 'object') {
                            acc[key] = parser.process(Object.keys(parser.xPath).reduce((subAcc, item) => {
                                subAcc[item] = new XPath(xpath.evaluate(
                                    parser.xPath[item],         // xpathExpression
                                    doc,                        // contextNode
                                    null,                       // namespaceResolver
                                    xpath.XPathResult.ANY_TYPE, // resultType
                                    null                        // result
                                ))
                                return subAcc
                            }, {}))
                        } else {
                            acc[key] = parser.process(new XPath(xpath.evaluate(
                                parser.xPath,               // xpathExpression
                                doc,                        // contextNode
                                null,                       // namespaceResolver
                                xpath.XPathResult.ANY_TYPE, // resultType
                                null                        // result
                            )))
                        }
                        return acc
                    }, {}), { link })))
                }
            })
        })
    }
}

module.exports = WebCrawler