let Crawler = require('./Crawler'),
    rp = require('request-promise'),
    request = require('request'),
    dom = require('xmldom').DOMParser,
    xpath = require('xpath')

class SitemapCrawler extends Crawler {
    constructor(config) {
        super(config)
        this.domOpts = {
            errorHandler: {
                warning: warning => {
                },
                error: error => {
                },
                fatalError: fatal => {
                }
            }
        }
    }
}

module.exports = SitemapCrawler