let request = require('request'),
    dom = require('xmldom').DOMParser,
    xpath = require('xpath'),
    { validate } = require('../utils/checks'),
    robotParser = require('robots-parser'),
    url = require('url')

class Crawler {
    constructor(config) {
        this.config = config

        validate(this.config)
        this.url = url.parse(this.config.params.url)
        this.robot = robotParser(`${this.url.protocol}//${this.url.host}/robots.txt`)
        this.status = {
            follow: [],
            scrape: [],
            ignore: [],
            disallow: [],
            processed: []
        }
    }

    isAllowed(link) {
        return this.robot.isAllowed(link, this.config.params.headers['User-agent'])
    }

    isScraped(link) {
        return (this.config.settings.scrape.length === undefined) ?
            link.match(this.config.settings.scrape) !== null :
            this.config.settings.scrape.reduce((acc, curr) => {
                return link.match(curr) || acc
            }, false)
    }

    isFollowed(link) {
        return (this.config.settings.follow.length === undefined) ?
            link.match(this.config.settings.follow) !== null :
            this.config.settings.follow.reduce((acc, curr) => {
                return link.match(curr) || acc
            }, false)
    }

    isIgnored(link) {
        if (this.config.settings.ignore === undefined) return false;
        return (this.config.settings.ignore.length === undefined) ?
            link.match(this.config.settings.ignore) !== null :
            this.config.settings.ignore.reduce((acc, curr) => {
                return link.match(curr) || acc
            }, false)
    }

    report() {
        console.log({
            followed: this.status.follow.length,
            scraped: this.status.scrape.length,
            ignored: this.status.ignore.length,
            disallowed: this.status.disallow.length,
            processed: this.status.processed.length
        })
    }
}

module.exports = Crawler