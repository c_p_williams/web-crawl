const crawlerTypes = require('./crawlers/types')

class Scraper {
    constructor(config) {
        if (Object.keys(crawlerTypes).includes(config.type)) {
            this.crawler = new crawlerTypes[config.type](config)
        } else {
            throw new Error(`Invalid crawler type - ${config.type}. Expected one of ${Object.keys(crawlerTypes)}`)
        }
    }

    start() {
        return this.crawler.start()
    }
}

module.exports = Scraper