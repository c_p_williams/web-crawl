let r = require('request')

module.exports = {
    validate: obj => {
        let required = [
            { key: 'name', type: 'string' },
            { key: 'type', type: 'string' },
            { key: 'params', type: 'object' },
            { key: 'delay', type: 'number' },
            { key: 'settings', type: 'object' },
            { key: 'parse', type: 'object' },
            { key: 'output', type: 'object' },
            { key: 'robots', type: 'object' }
        ]
        let has = Object.keys(obj)
        return required.reduce((acc, item) => {
            if (!has.includes(item.key)) {
                throw new Error(`Crawler: ${obj.name} is missing config: ${item.key}`)
            }
            else if (!(typeof obj[item.key] === item.type)) {
                throw new Error(`Crawler: ${obj.name} has invalid config type for: ${item.key}\nRequired: ${item.type}, Found: ${typeof obj[item.key]}`)
            }
            return acc && true
        }, true)
    },
    checkParser: (name, parser) => {
        let required = [
            'xPath',
            'process'
        ]
        let has = Object.keys(parser)
        required.forEach(item => {
            if (!has.includes(item))
                throw new Error(`Parser: ${name} has no value for: ${item}`)
        })
    }
}