module.exports = {
    replaceFrac: str => {
        let start = str.indexOf('&amp;frac')
        if (start > -1) {
            let whole = str.substr(0, start).trim()
            let frac = str.replace(whole, '').replace(/(&|&amp;)frac/g, '').trim()
            let following = str.split('frac')[1].split(';')[1].trim()
            return `${whole} ${frac[0]}/${frac[1]} ${following}`.trim()
        } else {
            return str
        }
    },
    replaceFrasl: str => {

    }
}