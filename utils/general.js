module.exports = {
    sleep: milliseconds => {
        let end = new Date().getTime() + milliseconds
        while (true) {
            if (new Date().getTime() >= end)
                return true
        }
    }
}