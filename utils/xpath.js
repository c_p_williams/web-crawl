module.exports = class XPath {
    constructor(val) {
        this.value = val
    }
    extract() {
        let iter = this.value.iterateNext()
        let arr = []
        while (iter) {
            arr.push(iter.toString())
            iter = this.value.iterateNext()
        }
        return arr
    }

    extract_first() {
        let iter = this.value.iterateNext()
        return iter.toString()
    }
}