const crawler = require('../crawlers/Crawler')
describe('Crawler test', () => {
    let Crawler;
    beforeAll(() => {
        Crawler = new crawler({
            name: 'crawler',
            type: 'WebCrawler',
            params: {
                url: 'example.com'
            },
            settings: {
                follow: [new RegExp('http:\/\/example.com/info'), new RegExp('http:\/\/example.com/viewAll')],
                scrape: new RegExp('http:\/\/example.com/data'),
                ignore: new RegExp('http:\/\/example.com/comments')
            },
            delay: 0,
            parse: null,
            output: null,
            robots: {}
        })
    })
    it('crawler should load correctly', () => {
        expect(Crawler).not.toBeNull()
    })
})