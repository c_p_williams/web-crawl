let checks = require('../utils/checks')
let parser = require('../utils/parser')
let xpath = require('../utils/xpath')
let general = require('../utils/general')
describe('Checks utilities', () => {
    it('should check crawler configs', () => {
        expect(checks.validate({
            name: 'Working',
            type: 'something',
            params: {},
            delay: 0,
            settings: {},
            parse: {},
            output: {},
            robots: {}
        })).toBe(true)
    })

    it('should check that a parser has the right format', () => {
        checks.checkParser('test crawler', {
            xPath: 'someString',
            process: function () { }
        })
    })

    it('should pause for a second', () => {
        let start = new Date().getTime()
        general.sleep(1000)
        expect(new Date().getTime()).toBeGreaterThanOrEqual(start + 1000)
    })
})