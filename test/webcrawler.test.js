let webCrawler = require('../crawlers/WebCrawler')

describe('WebCrawler test', () => {
    let crawler;
    beforeAll(() => {
        crawler = new webCrawler({
            name: 'web crawler',
            type: 'WebCrawler',
            params: {
                url: 'example.com'
            },
            settings: {
                follow: [new RegExp('http:\/\/example.com/info'), new RegExp('http:\/\/example.com/viewAll')],
                scrape: new RegExp('http:\/\/example.com/data'),
                ignore: new RegExp('http:\/\/example.com/comments')
            },
            delay: 0,
            parse: null,
            output: null,
            robots: {}
        })
    })

    it('should load correctly', () => {
        expect(crawler).not.toBeNull()
    })
})